
fs         = require 'fs'
extend     = require 'extend'
coreConfig = require './../core-config'

module.exports = class Config

	constructor: (modules) ->
		configPath = "#{modules.env.root}/config"
		env = modules.env.name
		config = {}
		try
			config = require configPath
			config = config[env] or {}
		catch err
			throw err if err.code isnt "MODULE_NOT_FOUND"
			try
				config = require "#{configPath}/#{env}"
				config ?= {}
			catch err
				throw err if err.code isnt "MODULE_NOT_FOUND"
		config = extend true, coreConfig, config
		extend true, @, config
			
			