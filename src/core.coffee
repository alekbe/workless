
coreModules = require './core-modules'

module.exports = class Core

    constructor: (options = {}) ->
        options.modules ?= []
        options.modules = coreModules.concat options.modules
        @modules = {}
        @loadModule modulePath for modulePath in options.modules
	
    loadModule: (modulePath) =>
        Module = require modulePath
        moduleName = Module.name.toLowerCase()
        @modules[moduleName] = new Module @modules