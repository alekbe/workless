
Sequelize = require 'sequelize'

module.exports = class Db

	constructor: (modules) ->
		sources = modules.config.db or {}
		for own sourceName, sourceParams of sources
			if !dbLoaders[sourceParams.source]
				modules.error.fatal new Error "Database source '#{sourceParams.source}' is not supported"
			@[sourceName] = dbLoaders[sourceParams.source] sourceParams.url, sourceParams.options
			
			
dbLoaders =
	postgres: (url, options = {}) -> loadSequelizeDatabase url, 'postgres', options
	mysql: (url, options = {}) -> loadSequelizeDatabase url, 'mysql', options
		
loadSequelizeDatabase = (url, dialect, options = {}) ->
	options.dialect = dialect
	db = new Sequelize url, options
	db