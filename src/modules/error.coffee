
restify = require 'restify'
_       = require 'underscore'

module.exports = class Error

	constructor: (modules) ->
		@log = modules.log
		process.on 'uncaughtException', (err) ->
			modules.log.error err, 'uncaughtException'
			process.exit 1
			
	fatal: (err) ->
		@log.error err, 'Fatal error'
		process.exit 1
		
	format: (err) ->
		return err if !err
		if err and err.errno is 1062
			err = new restify.ConflictError 'Unique contraint failed'
		else if err and err.code == '23503'
			err = new restify.ResourceNotFoundError 'Relation constraint failed'
		else if not (err instanceof Error) and _.isObject err
			_err = new restify.InvalidContentError 'Validation error'
			_err.errors = err
			err = _err
		err
		