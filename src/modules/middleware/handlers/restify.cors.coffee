
restify = require 'restify'

module.exports = (modules, config) ->
	
	crossOrigin = (req,res,next) ->
		res.header("Access-Control-Allow-Origin", "*")
		res.header("Access-Control-Allow-Headers", "X-Requested-With")
		next()
	restify.CORS.ALLOW_HEADERS.push 'authorization'
	cors = restify.CORS(config)
	fullResponse = restify.fullResponse()
	[cors,fullResponse]
	
module.exports.pre = true