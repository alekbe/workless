
path = require 'path'

module.exports = class Env

	constructor: (modules) ->
		@name = process.env.NODE_ENV or 'dev'
		@root = path.dirname process.mainModule.filename