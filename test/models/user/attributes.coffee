
Sequelize = require 'sequelize'

module.exports =
	
	id:
		type: Sequelize.INTEGER(11)
		autoIncrement: true
		primaryKey: true
	
	s1:
		type: Sequelize.STRING
		description: 's1 desc'
		notes: 's1 notes'
		defaultValue: 'abc'
	s2: Sequelize.STRING(1234)               
	s3: Sequelize.STRING.BINARY             
	s4: Sequelize.TEXT
	s5: Sequelize.ENUM('value 1', 'value 2')
	s6: Sequelize.UUID    
	 
	i1: Sequelize.INTEGER                     
	i2: Sequelize.BIGINT                      
	i3: Sequelize.BIGINT(11)                  
	
	f1: Sequelize.FLOAT                       
	f2: Sequelize.FLOAT(11)                  
	f3: Sequelize.FLOAT(11, 12)              
	 
	f4: Sequelize.DECIMAL                   
	f5: Sequelize.DECIMAL(10, 2)
	 
	d: Sequelize.DATE                       
	b: Sequelize.BOOLEAN              
	 
	
	#ar1: Sequelize.ARRAY(Sequelize.TEXT)
	 
	by1: Sequelize.BLOB                    
	by2: Sequelize.BLOB('tiny')
	by4: Sequelize.BLOB('medium')
	by5: Sequelize.BLOB('long')
	
	uint1: Sequelize.INTEGER.UNSIGNED
	uint2: Sequelize.INTEGER(11).UNSIGNED
	intz1: Sequelize.INTEGER(11).ZEROFILL         
	uintz2: Sequelize.INTEGER(11).ZEROFILL.UNSIGNED 
	uintz3: Sequelize.INTEGER(11).UNSIGNED.ZEROFILL 
	                 
	
		