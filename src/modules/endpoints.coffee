
requireDir = require 'require-directory'
restify    = require 'restify'

methodMap =
	'create': 'post'
	'read': 'get'
	'list': 'get'
	'delete': 'del'
	'update': 'put'

module.exports = class Endpoints

	constructor: (modules) ->
		@resources = {}
		endpointsPath = "#{modules.env.root}/endpoints"
		endpoints = {}
		try
			endpoints = require endpointsPath
		catch err
			throw err if err.code isnt  "MODULE_NOT_FOUND"
			try
				endpoints = requireDir module, endpointsPath
			catch err
				throw err if err.code isnt "ENOENT"
				
		@loadEndpoints version, resources, modules for own version, resources of endpoints
	
	authHandler: (auth) ->
		err = new restify.NotAuthorizedError 'Not Authorized'
		(req,res,next) ->
			return next err if !req.token
			for own key, val of auth
				return next err if req.token[key] isnt val
			next()
		
	loadEndpoints: (version, resources, modules) =>
		# check version validity
		for own resourceName, resourceData of resources
			for own endpoint, endpointData of resourceData
				[endpointAction] = endpoint.split('.').reverse()
				endpointData.path ?= ''
				endpointData.action ?= endpointAction
				endpointData.handler ?= (req,res,next) -> next()
					
				for action in endpointData.action.split ' '
					modules.error.fatal new Error "Endpoint action '#{action}' is not supported" if !methodMap[action]
					method = methodMap[action.toLowerCase()]
					prefix = modules.config.api?.prefix or ''
					path = "#{prefix}/#{resourceName}#{endpointData.path}"
					handlers = [endpointData.handler]
					
					if endpointData.tokenAuth
						auth = endpointData.tokenAuth
						if auth is true
							handlers.unshift (req,res,next) ->
								return next new restify.NotAuthorizedError 'Not Authorized' if !req.token
								next()
						else
							handlers.unshift @authHandler auth
					if !endpointData.hidden			
						modules.server[method]({ path: path, version: version },handlers)
						@resources[version] ?= {}
						@resources[version][resourceName] ?= []
						@resources[version][resourceName].push
							data: endpointData
							path: path
							method: method
							relPath: "/#{resourceName}#{endpointData.path}"
						modules.log.info "Endpoint #{method.toUpperCase()} #{path} loaded (version = #{version})"
				