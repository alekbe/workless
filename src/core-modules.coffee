
module.exports = [
	'./modules/env'
	'./modules/config'
	'./modules/log'
	'./modules/error'
	'./modules/server'
	'./modules/middleware'
	'./modules/db'
	'./modules/models'
	'./modules/swagger-models'
	'./modules/endpoints'
	'./modules/swagger'
	'./modules/swagger-ui'
]