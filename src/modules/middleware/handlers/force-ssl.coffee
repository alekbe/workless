
module.exports = (modules,config) ->
	(req,res,next) ->
		if req.headers['x-forwarded-proto'] is 'https'
			req.isSecure = () -> true
			
		if !req.isSecure()
			res.header 'Location', "https://#{req.headers.host}#{req.url}"
			res.send 302
		else
			next()
		
module.exports.pre = true
