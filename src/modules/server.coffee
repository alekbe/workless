
restify = require 'restify'

module.exports = class Server

	constructor: (modules) ->
		
		serverConfig = modules.config.server or {}
		serverConfig.log = modules.log
		
		serverConfig.formatters =
			'application/json': jsonFormatter
		
		server = restify.createServer serverConfig
		
		server.on 'uncaughtException', (req, res, route, err) ->
			modules.log.error err 'server.uncaughtException'
			res.json 500, err
		
		server.start = () ->
			port = serverConfig.port
			server.listen port, (err) ->
				throw err if err
				modules.log.info "Server listening on port #{port}"
		return server
	

jsonFormatter = (req, res, body) ->
	if (body instanceof Error)
		res.statusCode = body.statusCode or 500
	
		if res.statusCode is 400 and body.body and body.errors
			body.body.errors = body.errors
		if body.body
			body = body.body
		else
			body =
				message: body.message
	else if Buffer.isBuffer body
		body = body.toString 'base64'
    

	data = JSON.stringify body
	res.setHeader 'Content-Length', Buffer.byteLength data
	data
