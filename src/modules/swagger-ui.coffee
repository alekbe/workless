
restify = require 'restify'

module.exports = class SwaggerUi

	constructor: (modules) ->
		if modules.config.swagger?.ui
			modules.server.get /.*/, restify.serveStatic
				directory: "#{__dirname}/../../swagger-ui"
				default: 'index.html'

	