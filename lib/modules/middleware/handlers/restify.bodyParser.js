// Generated by CoffeeScript 1.6.3
(function() {
  var restify, _;

  restify = require('restify');

  _ = require('underscore');

  module.exports = function(modules, config) {
    return restify.bodyParser(config);
  };

}).call(this);
