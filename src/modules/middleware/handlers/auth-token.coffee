
clientSessions = require 'client-sessions'
cookieParser   = require 'cookie'

module.exports = (modules,config) ->
	modules.error.fatal new Error "config.token.secret is required for auth token middleware" if !modules.config.token?.secret
	opts = modules.config.token or {}
	opts.cookieName or= 'token-cookie'
	useCookie = modules.config.token?.useCookie or false
	
	createToken = (data, duration, createdAt) ->
		clientSessions.util.encode opts, data, duration, createdAt
		
	decodeToken = (token) ->
		clientSessions.util.decode opts, token
	
	(req,res,next) ->
		req.token = null
		req.createToken = (data, duration, createdAt) ->
			token = createToken data, duration, createdAt
			if useCookie and token
				res.setHeader 'set-cookie', "#{opts.cookieName}=#{token}" # TODO cookie options
			token
		req.decodeToken = decodeToken
		authHeader = req.headers['authorization']
		if !authHeader and useCookie
			try
				authHeader = cookieParser.parse(req.header 'cookie')[opts.cookieName]
		if authHeader
			try
				token = authHeader#.substr(7)
				data = decodeToken token
				req.token = if data then data.content else null
			catch err
				modules.log.error err, "Auth token parse error"
				
		next()
		
module.exports.pre = true