
requireDir = require 'require-directory'
_          = require 'underscore'

module.exports = class Middleware

	constructor: (modules) ->
		@middlewares = {}
		middlewarePaths = ["#{__dirname}/handlers","#{modules.env.root}/middleware"]
		@loadMiddleware middlewarePath, modules for middlewarePath in middlewarePaths
		middlewaresToLoad = modules.config.middleware or []
		
		@applyMiddleware middleware, modules for middleware in middlewaresToLoad
	
	loadMiddleware: (middlewarePath,modules) =>
		try
			middlewares = requireDir module, middlewarePath
			middlewares = middlewares.index if middlewares.index
			@middlewares = _.extend @middlewares, middlewares
		catch err
			throw err if err.code isnt 'ENOENT'
			return

	applyMiddleware: (middleware,modules) ->
		[middlewareName, params...] = middleware
		modules.error.fatal new Error "Middleware '#{middlewareName}' was not found" if !@middlewares[middlewareName]
		method = if @middlewares[middlewareName].pre then 'pre' else 'use'
		handlers = @middlewares[middlewareName].apply @, [modules].concat params
		handlers = [handlers] if !_.isArray handlers
		@_applyHandler method, handler, middlewareName, modules for handler in handlers
		modules.log.info "Middleware '#{middlewareName}' loaded (method = #{method})"
		
	_applyHandler: (method, handler, middlewareName, modules) ->
		modules.server[method] handler