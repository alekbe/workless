
bunyan = require 'bunyan'

module.exports = class Log

	constructor: (modules) ->
		logConfig = modules.config.log or {}
		log = bunyan.createLogger logConfig
		log.info "Loading app (env = #{modules.env.name})"
		return log