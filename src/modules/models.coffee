
requireDir = require 'require-directory'
inflect    = require 'inflect'
_          = require 'underscore'

module.exports = class Models

	constructor: (modules) ->
		@__assoc = {}
		modelsPath = "#{modules.env.root}/models"
		models = {}
		try
			models = require modelsPath
		catch err
			throw err if err.code isnt  "MODULE_NOT_FOUND"
			try
				models = requireDir module, modelsPath
			catch err
				throw err if err.code isnt "ENOENT"
		@loadModel modelKey, modelData, modules for own modelKey, modelData of models
		for own model, assoc of @__assoc
			for own rel, relData of assoc
				for model2 in relData
					model2 = { model: model2 } if _.isString model2
					opts = _.pick model2, ['foreignKeyConstraint','onDelete','onUpdate']
					m1 = @[model]
					m2 = @[model2.model]
					modules.log.info "ModelAssociation #{model} #{rel} #{model2.model} loaded"
					m1[rel] m2, opts
	loadModel: (modelKey, modelData, modules) =>
		[modelName, sourceName] = modelKey.split('.').reverse()
		modelName = inflect.camelize modelName
		sourceName or= modules.config.models?.defaultDb or 'default'
		try
			source = modules.config.db[sourceName].source
		catch err
			modules.error.fatal new Error "Database source '#{sourceName}' does not exist"
		if @[modelName]
			modules.error.fatal new Error "Duplicate model name '#{modelName}'"
		if !modelLoaders[source]
			modules.error.fatal new Error "Model source '#{source}' not supported"
		modelData.attributes ?= {}
		modelData.methods ?= {}
		modelData.statics ?= {}
		modelData.hooks ?= {}
		modelData.options ?= {}
		db = modules.db[sourceName]
		@[modelName] = modelLoaders[source] db, modelName, modelData, @__assoc
		modules.log.info "Model '#{modelName}' loaded (db = #{sourceName}, source = #{source})"
		
modelLoaders =
	postgres: (db, modelName, modelData, assoc) -> loadSequelizeModel db, 'postgres', modelName, modelData, assoc
	mysql: (db, modelName, modelData, assoc) -> loadSequelizeModel db, 'mysql', modelName, modelData, assoc
		
loadSequelizeModel = (db, dialect, modelName, modelData, assoc) ->
	modelData.options.instanceMethods = modelData.methods
	modelData.options.classMethods = modelData.statics
	modelData.options.hooks = modelData.hooks
	
	rel = ['hasMany','belongsTo']
	assoc[modelName] = {}
	for own key, val of modelData.attributes
		if key in rel
			delete modelData.attributes[key]
			assoc[modelName][key] = val
	model = db.define modelName, modelData.attributes, modelData.options
	model

		
		
			
		