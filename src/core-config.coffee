
module.exports =
	
	log:
		name: 'app-log'
		level: 'debug'
		
	server:
		name: 'app-server'
		port: process.env.PORT or '5000'
		version: '0.0.1'
		
	api:
		prefix: '/api'
		#formats:
			#'json': 'application/json'
			#'xml': 'application/xml'
		
	middleware: [
		#[ 'force-ssl' ]
		#[ 'auth-token' ]
		#[ 'api-format' ]
		#[ 'restify.cors' ]
		#[ 'restify.acceptParser' ]
		#[ 'restify.authorizationParser' ]
		#[ 'restify.dateParser' ]
		#[ 'restify.queryParser' ]
		#[ 'restify.jsonp' ]
		#[ 'restify.gzipResponse' ]
		#[ 'restify.bodyParser' ]
		#[ 'restify.requestLogger' ]
		#[ 'modules' ]
	]
		
	models:
		defaultDb: 'default'
		
	token:
		cookieName: 'api-token'
		
	swagger:
		version: '1.2'
		discover: '/api-docs'
		title: 'This is API doc title - config.swagger.title'
		description: 'This is API doc description - config.swagger.description'
		ui: true
