
module.exports =
	
	path: '/:id'
	
	inputClass: 'User'
	inputFilter: []
	outputClass: 'User'
	outputFilter: []
	
	params:
		id:
			source: 'path'
			type: 'string'
			desc: 'User id'
	
	handler: (req,res,next) ->
		res.json { ok: 'yes2' }
		
	nickname: 'getUserByID2'
	summary: 'summary2'
	notes: 'notes2'
	