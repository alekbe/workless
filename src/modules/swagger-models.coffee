
_ = require 'underscore'

types =
	integer: ['integer','bigint']
	string: ['varchar','text','enum','uuid']
	float: ['float','decimal']
	boolean: ['tinyint(1)']
	date: ['datetime']
	byte: ['blob']

module.exports = class SwaggerModels

	constructor: (modules) ->
		swaggerModels = {}
		for own modelName, model of modules.models
			continue if _.isFunction model	
			swaggerModels[modelName] = @loadSwaggerModel modelName, model, modules
			modules.log.info "SwaggerModel '#{modelName}' loaded"
		return swaggerModels
		
	loadSwaggerModel: (name, data, modules) =>
		#source = data.source
		attributes = data.rawAttributes
		source = 'mysql' # sequelize only
		if source in ['mysql','postgres','sqlite','mariadb']
			return @loadSequelizeModel name, source, attributes, modules
		else if source is 'mongodb'
			return @loadMongoModel name, source, attributes, modules
		else
			modules.error.fatal new Error "Swagger model not supported for source '#{source}'"
		
	loadSequelizeModel: (name, source, attributes, modules) =>
		swaggerModel = { id: name, properties: {} }
		for own attr, params of attributes
			swType = @_getType params
			modules.error.fatal new Error "Unsupported type of attribute '#{attr}' in model '#{name}(#{source})'" if swType is null
			length = params._length or params.type?._length
			obj = { name: attr, type: swType }
			obj.length = length if length
			swaggerModel.properties[obj.name] =
				type: obj.type
				description: params.description or ''
				notes: params.notes or ''
				required: (!params.allowNull and params.defaultValue is undefined)
		swaggerModel
		
	loadMongoModel: (name, source, attributes) =>
		
	_getType: (field) =>
		#console.error field
		checkType = null
		if field._typeName and _.isString field._typeName
			checkType = field._typeName
		else if field.type and field.type._typeName and _.isString field.type._typeName
			checkType = field.type._typeName
		else if field.type and _.isString field.type
			checkType = field.type
		else if _.isString field
			checkType = field
		if checkType and _.isString checkType
			checkType = checkType.toLowerCase()
			for own key, values of types
				return key if checkType in values
		return 'byte' if field._binary
		return 'string'
		return null