
restify = require 'restify'

defaultFormats =
	'json': 'application/json'
	'xml': 'application/xml'

module.exports = (modules,config = {}) ->
	config.prefix ?= modules.config.api?.prefix or '/api'
	config.formats ?= modules.config.api?.formats or defaultFormats
	
	#versionPattern = new RegExp "^#{config.prefix}/(v((?:(?:[0-9]+|x))(?:.(?:[0-9]+|x)(?:.(?:[0-9]+|x))?)?)(?:\/|$))"
	versionPattern = new RegExp "^#{config.prefix}(?:v((?:(?:[0-9]+|x))(?:.(?:[0-9]+|x)(?:.(?:[0-9]+|x))?)?)?/)?(?:(.*?)(?:\\.(.*?))?(?:$|/))?(.*?)$"
	
	handler = (req,res,next) ->
		matches = versionPattern.exec req.url
		return next() if !matches
		[path,version,resource,format,rest] = matches
		if version
			version = version.split '.'
			version[1] ?= 'x'
			version[2] ?= 'x'
			req.headers['accept-version'] = version.join '.'
		if format
			return next new restify.NotAcceptableError "Format '#{format}' not supported" if !config.formats[format]
			req.headers.accept = config.formats[format]
		rest = if rest then "#{rest}" else ""
		req.url = "#{config.prefix}#{resource}#{rest}"
		next()
	handler
	
module.exports.pre = true