
_      = require 'underscore'
extend = require 'extend'

module.exports = class Swagger

	constructor: (modules) ->
		prefix = modules.config.api?.prefix or ''
		discover = modules.config.swagger.discover
		discover = "#{prefix}#{discover}"
		versions = modules.endpoints.resources
		@loadSwaggerVersion version, resources, discover, modules for own version, resources of versions
	
	loadSwaggerVersion: (version, resources, discover, modules) =>
		options =
			swaggerVersion: modules.config.swagger?.version
			apiVersion: version
			info:
				title: modules.config.swagger?.title
				description: modules.config.swagger?.description
			apis: []
		for own resourceName, resourceData of resources
			options.apis.push { path: "/#{resourceName}" }
		modules.server.get { path: discover, version: version }, (req, res, next) ->
			res.json options
		modules.log.info "SwaggerEndpoint '#{discover}' loaded (version = #{version})"
		for own resourceName, resourceData of resources
			@loadSwaggerResource resourceName, resourceData, version, discover, modules
	
	maskSwaggerModel: (model, modules) ->
		newModel = extend true, {}, modules.swaggermodels[model.name]
		#newModel.id = model.rename
		for own prop, propData of newModel.properties
			delete newModel.properties[prop] if prop not in model.properties
		modules.swaggermodels[model.rename] = newModel
		model.rename
	
	parseSwaggerModel: (model, modules) =>
		return model if _.isString model
		if _.isArray model
			model = @parseSwaggerModel model[0], modules
			collectionName = "#{model}Collection"
			if !modules.swaggermodels[collectionName]
				modules.swaggermodels[collectionName] =
					id: collectionName
					properties:
						items: { type: 'array', items: { $ref: model }, required: true }
			return collectionName
		else
			return model = @maskSwaggerModel model, modules
		model
		
	convertParams: (path) =>
		start = path.indexOf '/:'
		return path if start is -1
		path = path.replace '/:', '/{'
		stop = path.indexOf '/', start + 1
		return path + '}' if stop is -1
		left = path.substr 0, stop
		right = path.substr stop
		path = left + '}' +right
		return @convertParams path
		
	loadSwaggerResource: (resourceName, resourceData, version, discover, modules) =>
		options =
			apiVersion: version
			swaggerVersion: modules.config.swagger?.version
			resourcePath: "/#{resourceName}"
			basePath: ''
			#produces: []
			#consumes: []
			apis: []
			models: modules.swaggermodels
		prefix = modules.config.api?.prefix or ''
		path = "#{discover}/#{resourceName}"
		
		for endpoint in resourceData
			endpoint.method = 'delete' if endpoint.method is 'del'

			options.apis.push
				path: @convertParams endpoint.relPath
				operations: [{
					method: endpoint.method.toUpperCase(),
					summary: endpoint.data.summary or ''
					notes: endpoint.data.notes or ''
					type: if endpoint.data.outputModel then @parseSwaggerModel endpoint.data.outputModel, modules else 'void'
					nickname: endpoint.data.nickname
					parameters: endpoint.data.params or []
					responseMessages: endpoint.data.responses or []
				}]
			api = _.last options.apis
			operation = api.operations[0]
			if endpoint.data.inputModel
				model = endpoint.data.inputModel
				endpoint.data.inputModel = @parseSwaggerModel model, modules
				realName = modules.swaggermodels[endpoint.data.inputModel].id
				operation.parameters.unshift
					name: realName
					description: "#{realName}Model"
					required: true
					type: endpoint.data.inputModel
					paramType: 'body'
			globalParams = modules.config.swagger?.globalParameters or []
			for globalParam in globalParams
				#console.error globalParam
				operation.parameters.push globalParam
			globalResponses = modules.config.swagger?.globalResponses or []
			for globalResponse in globalResponses
				#console.error globalParam
				operation.responseMessages.push globalResponse
			
					
		modules.server.get { path: path, version: version }, (req,res,next) ->
			protocol = if req.isSecure() then 'https' else 'http'
			currentVersion = if req.headers['accept-version'] then "/v#{req.headers['accept-version']}" else ""
			basePath = "#{protocol}://#{req.headers.host}#{prefix}#{currentVersion}"
			res.json _.extend options, { basePath: basePath }
		
		modules.log.info "SwaggerEndpoint '#{path}' loaded (version = #{version})"
	
			